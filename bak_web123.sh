#!/bin/bash

#instance ID
instance=$USER

# Database credentials
user="${instance}_live"
password="zzA8rzVu4gclaMaG"
host="localhost"
db_name="${instance}_live"

# Other options
backup_path="/home/${instance}/www/wp-content/database"
date=$(date +"%d-%b-%Y")

# Set default file permissions
umask 002

# Dump database into SQL file
mysqldump --user=$user --password=$password --host=$host $db_name > $backup_path/$db_name-$date.sql

#rm old DB files
find $backup_path -mtime +7 -exec rm -f {} \;

# Git: add and commit changes
cd /home/$instance/www/wp-content && /usr/bin/git add .
cd /home/$instance/www/wp-content && /usr/bin/git commit -a -m "daily crontab backup `date`"

# send data to Git server
cd /home/$instance/www/wp-content && /usr/bin/git push origin master


# MAILTO="dev+cronjob@web123.com.au"
# 5 0 * * * /home//www/wp-content/bak_web123.sh
